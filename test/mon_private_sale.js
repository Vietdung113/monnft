const MONPrivateSale = artifacts.require("MONPrivateSale");
const MONToken = artifacts.require("MONToken");
const BuyingToken = artifacts.require("MyToken");
const { BN, expectRevert, time } = require("@openzeppelin/test-helpers");

const Web3 = require("web3");
const web3 = new Web3();

/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
let MONPrivateSaleContract;
let MonTokenContract;
let buyingTokenContract;

contract("MonPrivateSale", function (accounts) {
  before(async function () {
    MonTokenContract = await MONToken.new();
    buyingTokenContract = await BuyingToken.new();
    MONPrivateSaleContract = await MONPrivateSale.new(
      MonTokenContract.address,
      buyingTokenContract.address
    );
    // set sale info
    let price = web3.utils.toWei("2000000000", "wei");
    let minSpend = web3.utils.toWei("10", "ether");
    let maxSpend = web3.utils.toWei("100", "ether");
    let startTime = await time.latest();
    let endTime = await startTime.add(time.duration.days(3));
    await MONPrivateSaleContract.setSaleInfo(
      price,
      minSpend,
      maxSpend,
      startTime,
      endTime
    );
    // set claimable
    let claimablieTimeStamps = [
      (await time.latest()).add(time.duration.days(4)),
    ];
    await MONPrivateSaleContract.setClaimableTimes(claimablieTimeStamps);
    let claimPercentage = [100];
    await MONPrivateSaleContract.setClaimablePercents(
      claimablieTimeStamps,
      claimPercentage
    );
    ///
    await buyingTokenContract.mint(
      accounts[0],
      web3.utils.toWei("10000", "ether")
    );

    await MonTokenContract.mint(
      MONPrivateSaleContract.address,
      web3.utils.toWei("10000000", "ether")
    );
  });
  describe("Test buy", async () => {
    it("Buy some tokens", async () => {
      let buyAmount = web3.utils.toWei("100", "ether");
      const DECIMAL_PRICE = new BN(
        await MONPrivateSaleContract.DECIMAL_PRICE(),
        10
      );
      let price = new BN(await MONPrivateSaleContract.priceToken(), 10);
      let buyingBalance = await buyingTokenContract.balanceOf(accounts[0]);
      // buy some MonToken by account 1
      await buyingTokenContract.approve(
        MONPrivateSaleContract.address,
        buyingBalance.toString(),
        { from: accounts[0] }
      );

      await MONPrivateSaleContract.buy(buyAmount, { from: accounts[0] });
      monBalanceOfAccount = await MONPrivateSaleContract.userBought(
        accounts[0]
      );

      let expectBalanceBought = new BN(buyAmount, 10)
        .mul(DECIMAL_PRICE)
        .div(price);
      assert.equal(
        expectBalanceBought.toString(),
        monBalanceOfAccount.toString()
      );
      // claim Montoken to Account
      time.increase(time.duration.days(5));
      await MONPrivateSaleContract.claim({ from: accounts[0] });
      let userClaimed = await MONPrivateSaleContract.userClaimned(accounts[0]);
      let monBalanceOfAccounts = await  MonTokenContract.balanceOf(accounts[0]);
      console.log(monBalanceOfAccounts.toString());
      assert.equal(monBalanceOfAccounts.toString(),userClaimed.toString());
    });
  });
  it("should assert true", async function () {
    await MONPrivateSale.deployed();
    return assert.isTrue(true);
  });
});
