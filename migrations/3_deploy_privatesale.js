const MONPrivateSale = artifacts.require("MONPrivateSale");
let BUSDAddress = '0x8301f2213c0eed49a7e28ae4c3e91722919b8b47';
let MONTokenAddress = '0xDd289AF12b4cC46e6E20860e2b530271c4991d13';
module.exports = function (deployer) {
  deployer.deploy(MONPrivateSale, MONTokenAddress, BUSDAddress);
};
